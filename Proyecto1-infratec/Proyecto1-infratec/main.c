/*
PROYECTO 1 FUNDAMENTOS DE INFRAESTRUCTURA TECNOLOGICA - 201610
*/
//Luis Felipe Plazas 201425881 lf.plazas10@uniandes.edu.co
//Ingacio Gomez 201224340 i.gomez10@uniandes.edu.co
#include "stdlib.h"
#include "stdio.h"
#include "math.h"

typedef struct archivo
{
	int tamanho;
	unsigned char *informacion;
} ARCHIVO;

typedef struct bit
{
	unsigned int f : 4;
};



void cargarArchivo(ARCHIVO *info, ARCHIVO *resultado, char *nomArchivoEntrada);
void guardarArchivo(ARCHIVO *data, char *nomArchivoSalida);
int obtenerNumBits(unsigned short clave);
void codificar(ARCHIVO *arch, ARCHIVO *resultado, unsigned short claveBinaria, int tamanhoClave);


int main(int argc, char* argv[])
{
	//Declaración de variables
	unsigned short claveBinaria;
	unsigned int tamanhoClave;

	//Se comprueba que se ingresaran 2 argumentos
	if (argc != 3)
	{
		printf("Faltan argumentos - Debe ser: archivoEntrada archivoSalida\n");
		system("pause");
		return -1;
	}

	//Se reserva espacio para las variables ARCHIVO en las cuales se guarda el archivo de entrada y el archivo de salida
	ARCHIVO *arch = (ARCHIVO*)malloc(sizeof(ARCHIVO));//Archivo de entrada
	ARCHIVO *resultado = (ARCHIVO*)malloc(sizeof(ARCHIVO));//Archivo de salida


	//Se imprimen los archivos de entrada y salida
	printf("Archivo de entrada %s\n", argv[1]);
	printf("Archivo de salida %s\n", argv[2]);


	//Se obtiene la clave
	printf("Ingrese la clave: ");
	scanf_s("%hu", &claveBinaria);
	printf("Clave ingresada: %hu\n", claveBinaria);


	//Se obtiene la longitud de la clave
	tamanhoClave = obtenerNumBits(claveBinaria);

	printf("numero de bits: %d\n", tamanhoClave);

	if (tamanhoClave > 16) {
		printf("La clave no puede tener mas de 16 bits\n");
		system("pause");
		return -1;
	}



	//Se cargan los datos del archivo de entrada en la variable arch de tipo ARCHIVO
	cargarArchivo(arch, resultado, argv[1]);
	printf("the file was loaded succesfully\n");

	//Se codifica la informacion
	codificar(arch, resultado, claveBinaria, tamanhoClave);
	printf("the file was crypted succesfully\n");

	//Se guarda el archivo codificado
	guardarArchivo(resultado, argv[2]);
	printf("the file was saved succesfully\n");

	//Fin
	system("pause");
	return 0;
}


/*
* Retorna el bit en el indice especificado de la llave dada.
*/
int getBitIndex(unsigned short key, int index)
{
	int mask = 1 << index;
	int masked_n = key & mask;
	int thebit = masked_n >> index;
	return thebit;
}

/* Imprime en orden los bits encriptados
*/
void viewOutputBits(unsigned char* ptr, int size)
{
	unsigned char *destiny = ptr;
	for (int j = 0; j < size; j++)
	{
		unsigned char act = *destiny;
		for (int bit = 7; bit >= 0; bit--)
		{
			int thebit = getBitIndex(act, bit);
			printf_s("\n BIT: %d: \n", thebit);
		}
		destiny++;
	}
}

/*
* Procedimiento que codifica
*/
//TODO: DESARROLLAR COMPLETAMENTE ESTA FUNCION
void codificar(ARCHIVO *arch, ARCHIVO *resultado, unsigned short claveBinaria, int tamanhoClave)
{
	int size = arch->tamanho;						   //Tamaño del archivo de salida y entrada.
	unsigned char *info = arch->informacion;           //Apuntador que inicia en el primer carácter del arhivo de entrada.
	unsigned char *destiny = resultado->informacion;   //Apuntador que inicia en el primer carácter del arhivo de salida.
	int amount = 0;					                   //Contador del bit a usar de la clave.
	for (int i = 0; i < size; i++)					   //Ciclo que recorre todos los chars del archivo de salida y entrada, tienen el mismo tamaño.
	{
		unsigned char act = *info;	                   //Char actual de la cadena a codificar.
		if (act == NULL)					               //Fin de la cadena.
		{
			break;
		}
		char toPut = 00000000;	                       //Char a guardar en el arhivo resultante.
		for (int bit = 7; bit >= 0; bit--)	           //Recorrre los 8 bits del char actual a codificar.
		{
			int thebit = getBitIndex(act, bit);	       //Primer bit de la llave y a codificar.
			int cryptBit = getBitIndex(claveBinaria, tamanhoClave-1-(amount % tamanhoClave));
			toPut |= (thebit ^ cryptBit) << bit;       //Se codifica y se pone en la posición del char resultante.
			amount++;
		}
		*destiny = toPut;                              //Se guarda el char resultante en el arhivo a guardar.
		destiny++;
		info++;
	}

	// Quitar comentario para imprimir en orden los bits encryptados:
	//viewOutputBits(resultado->informacion, size);
}

/*
* Procedimiento para obtener el numero de bits de la contraseña, mueve bit pot bit hacia la derecha hasta que todo sea ceros.
*/
//TODO: DESARROLLAR COMPLETAMENTE ESTA FUNCION
int obtenerNumBits(char *clave) //TODO CAMBIO DE PARAMETRO
{
	asm(
		//G
		push ebx
		push ecx
		push esi
		//PROC
		mov esi,0
		mov ecx,0
		mov ecx , [ebp+8]
		mov ecx, [ecx]
		while1:
			mov al, [ecx + esi]
			cmp al,'0'
			je carsig:
			cmp al,'1'
			je carsig
			cmp al,0
			je fin:
			jmp error:
			carsig:
				inc esi
			jmp while1
		error:
			mov eax,0
		fin:
			mov eax, esi
		//G
		pop esi
		pop ecx
		pop ebx
		
		
	)
}

/*
* Procedimiento para cargar un archivo de disco
*/
//NO MODIFICAR
void cargarArchivo(ARCHIVO *data, ARCHIVO *resultado, char *nomArchivoEntrada)
{
	FILE *streamArchivo;
	int tam;
	unsigned char * aux1;
	unsigned char * aux2;

	if (!(streamArchivo = fopen(nomArchivoEntrada, "rb")))
	{
		printf("No se puede abrir el archivo: %s\n", nomArchivoEntrada);
		system("pause");
		exit(EXIT_FAILURE);
	}

	fseek(streamArchivo, 0, SEEK_END);
	tam = ftell(streamArchivo);
	fseek(streamArchivo, 0, SEEK_SET);

	aux1 = (unsigned char*)calloc(tam, 1);
	aux2 = (unsigned char*)calloc(tam, 1);

	fread(aux1, 1, tam, streamArchivo);

	data->tamanho = tam;
	data->informacion = aux1;

	resultado->tamanho = tam;
	resultado->informacion = aux2;

	if (fclose(streamArchivo) != 0)
	{
		printf("Error cerrando el archivo");
	}
}

/*
* Procedimiento para guardar un archivo en disco
*/
//NO MODIFICAR
void guardarArchivo(ARCHIVO *data, char *nomArchivoSalida)
{
	FILE *streamArchivo;

	if (!(streamArchivo = fopen(nomArchivoSalida, "wb"))) {
		printf("No se puede abrir el archivo: %s\n", nomArchivoSalida);
		system("pause");
		exit(EXIT_FAILURE);
	}

	fwrite((data->informacion), 1, (data->tamanho), streamArchivo);

	if (fclose(streamArchivo) != 0)
	{
		printf("Error cerrando el archivo");
	}
}
